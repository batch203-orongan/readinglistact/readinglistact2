// console.log("Hello, World!");

// Problem 1

function arrayOfNumbers(){
	let num1 =Number(prompt("Enter 1st number:"));
	let num2 =Number(prompt("Enter 2nd number:"));
	let num3 =Number(prompt("Enter 3rd number:"));
	let num4 =Number(prompt("Enter 4th number:"));
	let num5 =Number(prompt("Enter 5ft number:"));

	let numbers = [num1, num2, num3, num4, num5];
	console.log(numbers);

	let reduceNumber = numbers.reduce((acc, cur) => acc + cur);

	alert(`The total of the entered five numbers is ${reduceNumber}`);
};

arrayOfNumbers();

// Problem 2

let fruits = ["apple", "grapes", "orange", "mango", "apple", "strawberry", "grapes"];
let duplicate = [];

function duplicateFruits(){
	let sortedFruits = fruits.sort();

	for(let i = 0 ; i < sortedFruits.length-1; i++){
		if(sortedFruits[i] == sortedFruits[i+1]){
			duplicate.push(sortedFruits[i]);
		}
	}
};

duplicateFruits();
console.log(duplicate);

// problem 3

let numbers = [2, 4, 6, 7, 10];

let numberMap =numbers.map(x => x ** 3);
// console.log(numberMap);

for(let i = 0; i < numbers.length; i++){
	console.log(`The cube value of ${numbers[i]} is ${numberMap[i]}`)
};